export interface Auth {
    id: number,
    firstName: string,
    lastName: string,
    secondName: string,
    secondLastName: string,
    email: string,
    balance: number,
    createAt: Date,
    updateAt: Date,
}

export type AuthCreateInput = {
    firstName: string,
    lastName?: string,
    secondName?: string,
    secondLastName?: string,
    balance: number,
    email: string
}

export type AuthUpdateInput = {
    firstName?: string,
    lastName?: string,
    secondName?: string,
    secondLastName?: string,
    balance?: number,
    email?: string
}