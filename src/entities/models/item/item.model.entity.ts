export interface Item {
    id: number,
    title: string,
    description: string,
    price: number,
    quantity: number,
    createAt: Date,
    updateAt: Date,
    deletedAt: Date | null,
    auth?: string,
    purchases?: string;
}

export type ItemCreateInput = {
    title: string,
    description: string,
    price: number,
    quantity: number,
    auth: string,
}

export type ItemUpdateInput = {
    title?: string,
    description?: string,
    price?: number,
    quantity?: number
}