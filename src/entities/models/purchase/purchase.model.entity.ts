export interface Purchase {
    id: number,
    authId?: number,
    itemId?: number,
    createAt: Date,
    updateAt: Date,
    purchaseAt: Date
}

export type PurchaseCreateInput = {
    authId: number,
    itemId: number,
    purchaseAt: Date
}
export type PurchaseUpdateInput = {
    authId?: number,
    itemId?: number,
    updateAt?: Date
}