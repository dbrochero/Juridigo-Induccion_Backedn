import { Auth, AuthCreateInput, AuthUpdateInput } from "../../models/auth/auth.model.entity";

export interface IAuthUseCase {
    create(input: AuthCreateInput): Promise<Auth>;
    create<T>(input: AuthCreateInput, transactionManager: T): Promise<Auth>;
    update(id: number, input: AuthUpdateInput): Promise<Auth | undefined>;
    update<T>(id: number, input: AuthUpdateInput, transactionManager: T): Promise<Auth | undefined>;
    delete(id: number): Promise<Auth | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Auth | undefined>;
    getById(id: number): Promise<Auth | undefined>;
    getAll(): Promise<Array<Auth>>;
}