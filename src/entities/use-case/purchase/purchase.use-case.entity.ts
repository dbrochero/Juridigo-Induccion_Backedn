import { Purchase, PurchaseCreateInput, PurchaseUpdateInput } from "../..//models/purchase/purchase.model.entity";

export interface IPurchaseUseCase {
    create(input: PurchaseCreateInput): Promise<Purchase>;
    create<T>(input: PurchaseCreateInput, transactionManager: T): Promise<Purchase>;

    update(id: number, input: PurchaseUpdateInput): Promise<Purchase | undefined>;
    update<T>(id: number, input: PurchaseUpdateInput, transactionManager: T): Promise<Purchase | undefined>;

    delete(id: number): Promise<Purchase | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Purchase | undefined>;

    getById(id: number): Promise<Purchase | undefined>;
    getAll(): Promise<Array<Purchase>>;
}