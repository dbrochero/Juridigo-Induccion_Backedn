import { Item, ItemCreateInput, ItemUpdateInput } from "../../../entities/models/item/item.model.entity";
import { IItemUseCase } from '../../../entities/use-case/item/item.use-case.entity'
import { IItemRepository } from "../../ports/repositories/item/item.repository";

export class ItemUseCase implements  IItemUseCase {
    constructor(private readonly itemRepository: IItemRepository) {
    }

    create(input: ItemCreateInput): Promise<Item>;
    create<T>(input: ItemCreateInput, transactionManager: T): Promise<Item>;
    async create(input: ItemCreateInput, transactionManager?: any): Promise<Item> {
        let itemFound;
        await this.itemRepository.getAll().then(
            (items) => {
                itemFound = items.find(item => item.title === input.title)
            }
        )
        if ( itemFound !== undefined ){
            console.log("El usuario ya existe")
            //Insertar una alerta
        }

        if (transactionManager) {
            return this.itemRepository.create(input, transactionManager);
        } else {
            return this.itemRepository.create(input);
        }
    }

    update(id: number, input: ItemUpdateInput): Promise<Item | undefined>;
    update<T>(id: number, input: ItemUpdateInput, transactionManager: T): Promise<Item | undefined>;
    async update(id: number, input: ItemUpdateInput, transactionManager?: any): Promise<Item | undefined> {
        // Validate if id exist
        const authIdFound = this.itemRepository.getById(id) ;
        if (authIdFound === undefined){
            console.log("Usuario no encontrado")
        }

        // Validate update input
        if (transactionManager){
            return this.itemRepository.update(id, input, transactionManager)
        } else {
            return this.itemRepository.update(id, input)
        }

    }

    delete(id: number): Promise<Item | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Item | undefined>;
    delete(id: number, transactionManager?: any): Promise<Item | undefined>{
        const auth = this.itemRepository.getById(id);
        if(auth === undefined){
            throw new Error("Error user undefined")
        }
        if (transactionManager) {
            return this.itemRepository.delete(id, transactionManager);
        } else {
            return this.itemRepository.delete(id);
        }
    }

    getById(id: number): Promise<Item | undefined>{
        const auth = this.itemRepository.getById(id);
        if(auth === undefined){
            throw new Error("Error user undefined")
        }
        return auth;
    }

    getAll(): Promise<Item[]> {
        return this.itemRepository.getAll();
    }

}