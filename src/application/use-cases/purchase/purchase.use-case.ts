import { Purchase, PurchaseUpdateInput, PurchaseCreateInput } from "../../../entities/models/purchase/purchase.model.entity";
import { IPurchaseUseCase } from '../../../entities/use-case/purchase/purchase.use-case.entity'
import { IPurchaseRepository } from "../../ports/repositories/purchase/purchase.repository";

export class PurchaseUseCase implements  IPurchaseUseCase {
    constructor(private readonly purchaseRepository: IPurchaseRepository) {
    }

    create(input: PurchaseCreateInput): Promise<Purchase>;
    create<T>(input: PurchaseCreateInput, transactionManager: T): Promise<Purchase>;
    async create(input: PurchaseCreateInput, transactionManager?: any): Promise<Purchase> {

        if (transactionManager) {
            return this.purchaseRepository.create(input, transactionManager);
        } else {
            return this.purchaseRepository.create(input);
        }
    }

    update(id: number, input: PurchaseUpdateInput): Promise<Purchase | undefined>;
    update<T>(id: number, input: PurchaseUpdateInput, transactionManager: T): Promise<Purchase | undefined>;
    async update(id: number, input: PurchaseUpdateInput, transactionManager?:any): Promise<Purchase | undefined>{
        // Validate if id exist
        const authIdFound = this.purchaseRepository.getById(id) ;
        if (authIdFound === undefined){
            console.log("Usuario no encontrado")
        }

        // Validate update input
        if (transactionManager){
            return this.purchaseRepository.update(id, input, transactionManager)
        } else {
            return this.purchaseRepository.update(id, input)
        }
    }

    delete(id: number): Promise<Purchase | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Purchase | undefined>;
    delete(id: number, transactionManager?: any): Promise<Purchase | undefined>{
        const auth = this.purchaseRepository.getById(id);
        if(auth === undefined){
            throw new Error("Error user undefined")
        }
        if (transactionManager) {
            return this.purchaseRepository.delete(id, transactionManager);
        } else {
            return this.purchaseRepository.delete(id);
        }
    }

    getById(id: number): Promise<Purchase | undefined>{
        const auth = this.purchaseRepository.getById(id);
        if(auth === undefined){
            throw new Error("Error user undefined")
        }
        return auth;
    }

    getAll(): Promise<Purchase[]> {
        return this.purchaseRepository.getAll();
    }

}