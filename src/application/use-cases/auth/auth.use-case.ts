import { Auth, AuthUpdateInput, AuthCreateInput } from "../../../entities/models/auth/auth.model.entity";
import { IAuthUseCase } from '../../../entities/use-case/auth/auth.use-case.entity'
import { IAuthRepository } from "../../ports/repositories/auth/auth.repository";
//import { authErrorDescription } from '../../../presentation/controllers/topic/topic.controller';

export class AuthUseCase implements  IAuthUseCase {
    constructor(private readonly authRepository: IAuthRepository) {
    }

    create(input: AuthCreateInput): Promise<Auth>;
    create<T>(input: AuthCreateInput, transactionManager: T): Promise<Auth>;
    async create(input: AuthCreateInput, transactionManager?: any): Promise<Auth> {
        console.log("Create User")
        let authFound;
        await this.authRepository.getAll().then(
            (auths) => {
                authFound = auths.find(auth => auth.email === input.email)
            }
        )
        if ( authFound !== undefined ){
            console.log("El usuario ya existe")
            //Insertar una alerta
        }

        if (transactionManager) {
            return this.authRepository.create(input, transactionManager);
        } else {
            return this.authRepository.create(input);
        }
    }

    update(id: number, input: AuthUpdateInput): Promise<Auth | undefined>;
    update<T>(id: number, input: AuthUpdateInput, transactionManager: T): Promise<Auth | undefined>;
    async update(id: number, input: AuthUpdateInput, transactionManager?: any): Promise<Auth | undefined> {
        // Validate if id exist
        const authIdFound = this.authRepository.getById(id) ;
        if (authIdFound === undefined){
            console.log("Usuario no encontrado")
        }

        // Validate update input
        const auth: AuthUpdateInput = this.validateUpdateInput(input)
        if (transactionManager){
            return this.authRepository.update(id, auth, transactionManager)
        } else {
            return this.authRepository.update(id, auth)
        }

    }

    delete(id: number): Promise<Auth | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Auth | undefined>;
    delete(id: number, transactionManager?: any): Promise<Auth | undefined>{
        const auth = this.authRepository.getById(id);
        if(auth === undefined){
            throw new Error("Error user undefined")
        }
        if (transactionManager) {
            return this.authRepository.delete(id, transactionManager);
        } else {
            return this.authRepository.delete(id);
        }
    }

    getById(id: number): Promise<Auth | undefined>{
        const auth = this.authRepository.getById(id);
        if(auth === undefined){
            throw new Error("Error user undefined")
        }
        return auth;
    }

    getAll(): Promise<Auth[]> {
        return this.authRepository.getAll();
    }

    private validateUpdateInput(input: AuthUpdateInput): AuthUpdateInput {
        const {firstName, lastName, secondName, secondLastName, email} = input;
        let outputAuth: AuthUpdateInput = {};

        if (firstName !== undefined){
            if ( firstName.length<1){
                throw new Error("Error empty firstName")
            }
            if (firstName.length>100) {
                throw new Error("Error firstName firstName")
            }
            outputAuth.firstName = input.firstName;
        }
        if (lastName !== undefined){
            if (lastName.length>100) {
                throw new Error("Error firstName firstName")
            }
            outputAuth.lastName = input.lastName;
        }
        if (secondName !== undefined){
            if (secondName.length>100) {
                throw new Error("Error firstName firstName")
            }
            outputAuth.secondName = input.secondName;
        }
        if (secondLastName !== undefined){
            if (secondLastName.length>100) {
                throw new Error("Error firstName firstName")
            }
            outputAuth.secondLastName = input.secondLastName;
        }
        if (email !== undefined){
            if (email.length>100) {
                throw new Error("Error firstName firstName")
            }
            outputAuth.email = input.email;
        }
        return outputAuth
    }


}