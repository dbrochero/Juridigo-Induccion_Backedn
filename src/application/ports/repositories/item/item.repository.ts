import { Item, ItemCreateInput, ItemUpdateInput } from "../../../../entities/models/item/item.model.entity";
import { IRepository } from "../repository.port";

export interface IItemRepository extends  IRepository {
    create(input: ItemCreateInput): Promise<Item>;
    create<T>(input: ItemCreateInput, transactionManager: T): Promise<Item>;

    update(id: number, input: ItemUpdateInput): Promise<Item | undefined>;
    update<T>(id: number, input: ItemUpdateInput, transactionManager: T): Promise<Item | undefined>;

    delete(id: number): Promise<Item | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Item | undefined>

    getById(id: number): Promise<Item | undefined>;
    getAll(): Promise<Array<Item>>;
    getByTitle(title: string): Promise<Item | undefined>;
}