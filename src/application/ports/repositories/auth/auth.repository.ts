import { Auth, AuthCreateInput, AuthUpdateInput } from "../../../../entities/models/auth/auth.model.entity";
import { IRepository } from "../repository.port";

export interface IAuthRepository extends  IRepository {
    create(input: AuthCreateInput): Promise<Auth>;
    create<T>(input: AuthCreateInput, transactionManager: T): Promise<Auth>;

    update(id: number, input: AuthUpdateInput): Promise<Auth | undefined>;
    update<T>(id: number, input: AuthUpdateInput, transactionManager: T): Promise<Auth | undefined>;

    delete(id: number): Promise<Auth | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Auth | undefined>;

    getById(id: number): Promise<Auth | undefined>;
    getAll(): Promise<Array<Auth>>;
}