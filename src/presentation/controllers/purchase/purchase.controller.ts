import {Controller, ControllerAction} from "../../../application/ports/controllers/controller.ports";
import { PurchaseUseCase } from "../../../application/use-cases/purchase/purchase.use-case";
import {ResponseHandler, ResponseModel} from "../../../application/ports/responses/response.port";
import {RequestModel} from "../../../application/ports/requests/request.port";
import { Purchase } from "../../../entities/models/purchase/purchase.model.entity";

export type PurchaseCreateRequestInput = {
    authId: number,
    itemId: number,
    purchaseAt: Date
}

export type PurchaseUpdateRequestInput = {
    authId?: number,
    itemId?: number,
    updateAt?: Date
}

export  type PurchaseParamsRequestInput = {
    id: number;
}

export enum itemErrorDescription {
    BODY_NOT_PROVIDED = "Body was not provided",
    PARENT_TOPIC_NOT_FOUND = "Parent topic with given id does not exist",
    ID_NOT_PROVIDED = "Cannot start this operation without an id",
    TOPIC_NOT_FOUND = "The topic you are looking for does not exist",
    PARAM_WRONG_TYPE = "Some param or params do not have the correct type",
}

export class PurchaseController implements Controller{
    public static instance: PurchaseController;

    [name: string]: ControllerAction | unknown;

    constructor(private readonly purchaseUseCase: PurchaseUseCase, private readonly responseHandler: ResponseHandler) {
        PurchaseController.instance = this;
    }

    async create(request: RequestModel<PurchaseCreateRequestInput>): Promise<ResponseModel<Purchase>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(itemErrorDescription.BODY_NOT_PROVIDED);
            }
            const topic = await PurchaseController.instance.purchaseUseCase.create(request.body);
            return await PurchaseController.instance.responseHandler.response('success', 201, topic);
        } catch (error: any) {
            return await PurchaseController.instance.errorHandler(error)
        }
    }

    async update(request: RequestModel<PurchaseUpdateRequestInput, PurchaseParamsRequestInput>): Promise<ResponseModel<Purchase | undefined>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(itemErrorDescription.BODY_NOT_PROVIDED);
            }
            if (request.params?.id === undefined) {
                throw new Error(itemErrorDescription.ID_NOT_PROVIDED);
            }
            const updatedTopic = await PurchaseController.instance.purchaseUseCase.update(request.params?.id, request.body);
            return await PurchaseController.instance.responseHandler.response('success', 200, updatedTopic)
        } catch (error: any) {
            return await PurchaseController.instance.errorHandler(error);
        }
    }

    async delete(request: RequestModel<PurchaseParamsRequestInput>): Promise<ResponseModel<Purchase | undefined>> {
        try {
            if (request.params?.id === undefined) {
                throw new Error(itemErrorDescription.ID_NOT_PROVIDED);
            }
            const topic = await PurchaseController.instance.purchaseUseCase.getById(request.params.id)
            if (topic === undefined) {
                throw new Error(itemErrorDescription.TOPIC_NOT_FOUND);
            }
            await PurchaseController.instance.purchaseUseCase.delete(request.params.id)
            return await PurchaseController.instance.responseHandler.response("success", 202, topic)
        } catch (error: any) {
            return await PurchaseController.instance.errorHandler(error);
        }
    }

    async getAll(): Promise<ResponseModel<Purchase[]>> {
        try {
            const topics = await PurchaseController.instance.purchaseUseCase.getAll()
            return await PurchaseController.instance.responseHandler.response("success", 200, topics)
        } catch (error: any) {
            return await PurchaseController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    async getById(request: RequestModel<PurchaseParamsRequestInput>): Promise<ResponseModel<Purchase>> {
        try {
            if (request.params?.id === undefined) {
                throw new Error(itemErrorDescription.ID_NOT_PROVIDED)
            }
            const topic = await PurchaseController.instance.purchaseUseCase.getById(request.params?.id);
            if (topic !== undefined) {
                return await PurchaseController.instance.responseHandler.response("success", 200, topic)
            } else {
                throw new Error(itemErrorDescription.TOPIC_NOT_FOUND)
            }
        } catch (error: any) {
            if (error.message.includes(itemErrorDescription.TOPIC_NOT_FOUND)) {
                return await PurchaseController.instance.responseHandler.response('error', 404, error.message);
            }
            return await PurchaseController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    private async errorHandler(error: any): Promise<ResponseModel<Purchase>> {
        let code: number = 500;
        let errorMessage = error.message
        if (errorMessage === itemErrorDescription.TOPIC_NOT_FOUND || errorMessage === itemErrorDescription.PARENT_TOPIC_NOT_FOUND) {
            code = 404;
        } else if (Object.values(itemErrorDescription).includes(errorMessage)) {
            code = 400;
        } else if (errorMessage.includes("Syntax invalid")) {
            code = 400;
            errorMessage = itemErrorDescription.PARAM_WRONG_TYPE
        }
        return await PurchaseController.instance.responseHandler.response('error', code, errorMessage);
    }
}