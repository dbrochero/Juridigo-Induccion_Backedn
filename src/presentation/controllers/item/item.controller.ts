import {Controller, ControllerAction} from "../../../application/ports/controllers/controller.ports";
import { ItemUseCase } from "../../../application/use-cases/item/item.use-case";
import {ResponseHandler, ResponseModel} from "../../../application/ports/responses/response.port";
import {RequestModel} from "../../../application/ports/requests/request.port";
import { Item } from "../../../entities/models/item/item.model.entity";

export type ItemCreateRequestInput = {
    title: string,
    description: string,
    price: number,
    quantity: number,
    auth: string,
}

export type ItemUpdateRequestInput = {
    title: string,
    description: string,
    price: number,
    quantity: number,
    auth: string,
}

export  type ItemParamsRequestInput = {
    id: number;
}

export enum itemErrorDescription {
    BODY_NOT_PROVIDED = "Body was not provided",
    PARENT_TOPIC_NOT_FOUND = "Parent topic with given id does not exist",
    ID_NOT_PROVIDED = "Cannot start this operation without an id",
    TOPIC_NOT_FOUND = "The topic you are looking for does not exist",
    PARAM_WRONG_TYPE = "Some param or params do not have the correct type",
}

export class ItemController implements Controller{
    public static instance: ItemController;

    [name: string]: ControllerAction | unknown;

    constructor(private readonly itemUseCase: ItemUseCase, private readonly responseHandler: ResponseHandler) {
        ItemController.instance = this;
    }

    async create(request: RequestModel<ItemCreateRequestInput>): Promise<ResponseModel<Item>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(itemErrorDescription.BODY_NOT_PROVIDED);
            }
            const topic = await ItemController.instance.itemUseCase.create(request.body);
            return await ItemController.instance.responseHandler.response('success', 201, topic);
        } catch (error: any) {
            return await ItemController.instance.errorHandler(error)
        }
    }

    async update(request: RequestModel<ItemUpdateRequestInput, ItemParamsRequestInput>): Promise<ResponseModel<Item | undefined>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(itemErrorDescription.BODY_NOT_PROVIDED);
            }
            if (request.params?.id === undefined) {
                throw new Error(itemErrorDescription.ID_NOT_PROVIDED);
            }
            const updatedTopic = await ItemController.instance.itemUseCase.update(request.params?.id, request.body);
            return await ItemController.instance.responseHandler.response('success', 200, updatedTopic)
        } catch (error: any) {
            return await ItemController.instance.errorHandler(error);
        }
    }

    async delete(request: RequestModel<ItemParamsRequestInput>): Promise<ResponseModel<Item | undefined>> {
        try {
            if (request.params?.id === undefined) {
                throw new Error(itemErrorDescription.ID_NOT_PROVIDED);
            }
            const topic = await ItemController.instance.itemUseCase.getById(request.params.id)
            if (topic === undefined) {
                throw new Error(itemErrorDescription.TOPIC_NOT_FOUND);
            }
            await ItemController.instance.itemUseCase.delete(request.params.id)
            return await ItemController.instance.responseHandler.response("success", 202, topic)
        } catch (error: any) {
            return await ItemController.instance.errorHandler(error);
        }
    }

    async getAll(): Promise<ResponseModel<Item[]>> {
        try {
            console.log("DIERICK")
            const topics = await ItemController.instance.itemUseCase.getAll()
            return await ItemController.instance.responseHandler.response("success", 200, topics)
        } catch (error: any) {
            return await ItemController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    async getById(request: RequestModel<ItemParamsRequestInput>): Promise<ResponseModel<Item>> {
        try {
            if (request.params?.id === undefined) {
                throw new Error(itemErrorDescription.ID_NOT_PROVIDED)
            }
            const topic = await ItemController.instance.itemUseCase.getById(request.params?.id);
            if (topic !== undefined) {
                return await ItemController.instance.responseHandler.response("success", 200, topic)
            } else {
                throw new Error(itemErrorDescription.TOPIC_NOT_FOUND)
            }
        } catch (error: any) {
            if (error.message.includes(itemErrorDescription.TOPIC_NOT_FOUND)) {
                return await ItemController.instance.responseHandler.response('error', 404, error.message);
            }
            return await ItemController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    private async errorHandler(error: any): Promise<ResponseModel<Item>> {
        let code: number = 500;
        let errorMessage = error.message
        if (errorMessage === itemErrorDescription.TOPIC_NOT_FOUND || errorMessage === itemErrorDescription.PARENT_TOPIC_NOT_FOUND) {
            code = 404;
        } else if (Object.values(itemErrorDescription).includes(errorMessage)) {
            code = 400;
        } else if (errorMessage.includes("Syntax invalid")) {
            code = 400;
            errorMessage = itemErrorDescription.PARAM_WRONG_TYPE
        }
        return await ItemController.instance.responseHandler.response('error', code, errorMessage);
    }
}