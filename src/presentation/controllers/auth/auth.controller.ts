import {Controller, ControllerAction} from "../../../application/ports/controllers/controller.ports";
import { AuthUseCase } from "../../../application/use-cases/auth/auth.use-case";
import { RequestModel } from "../../../application/ports/requests/request.port";
import { ResponseHandler, ResponseModel } from "../../../application/ports/responses/response.port";
import { Auth } from "../../../entities/models/auth/auth.model.entity";

export type AuthCreateRequestInput = {
    firstName: string,
    lastName?: string,
    secondName?: string,
    secondLastName?: string,
    balance: number,
    email: string
}

export type AuthUpdateRequestInput = {
    firstName?: string,
    lastName?: string,
    secondName?: string,
    secondLastName?: string,
    balance?: number,
    email?: string
}

export  type AuthParamsRequestInput = {
    id: number;
}

export enum authErrorDescription {
    TITLE_FAILED_PARAM = "Title is mandatory, should not be empty",
    DESCRIPTION_FAILED_PARAM = "Description is mandatory, should not be empty",
    BODY_NOT_PROVIDED = "Body was not provided",
    TOPIC_ALREADY_EXIST = "A topic with the same title already exist",
    PARENT_TOPIC_NOT_FOUND = "Parent topic with given id does not exist",
    TITLE_TOO_LONG = "Title too long, it should not have a length greater than 500 characters",
    ID_NOT_PROVIDED = "Cannot start this operation without an id",
    TOPIC_NOT_FOUND = "The topic you are looking for does not exist",
    PARAM_WRONG_TYPE = "Some param or params do not have the correct type",
    PARENT_EQUALS_TO_CURRENT = "Parent topic should be different to current topic"
}

export class AuthController implements Controller{
    public static instance: AuthController;

    [name: string]: ControllerAction | unknown;

    constructor(private readonly authUseCase: AuthUseCase, private readonly responseHandler: ResponseHandler) {
        console.log("auth.controllers")
        AuthController.instance = this;
    }

    async create(request: RequestModel<AuthCreateRequestInput>): Promise<ResponseModel<Auth>> {
        console.log("Create")
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(authErrorDescription.BODY_NOT_PROVIDED);
            }
            const topic = await AuthController.instance.authUseCase.create(request.body);
            return await AuthController.instance.responseHandler.response('success', 201, topic);
        } catch (error: any) {
            return await AuthController.instance.errorHandler(error)
        }
    }

    async update(request: RequestModel<AuthUpdateRequestInput, AuthParamsRequestInput>): Promise<ResponseModel<Auth | undefined>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(authErrorDescription.BODY_NOT_PROVIDED);
            }
            if (request.params?.id === undefined) {
                throw new Error(authErrorDescription.ID_NOT_PROVIDED);
            }
            const updatedTopic = await AuthController.instance.authUseCase.update(request.params?.id, request.body);
            return await AuthController.instance.responseHandler.response('success', 200, updatedTopic)
        } catch (error: any) {
            return await AuthController.instance.errorHandler(error);
        }
    }

    async delete(request: RequestModel<AuthParamsRequestInput>): Promise<ResponseModel<Auth | undefined>> {
        try {
            if (request.params?.id === undefined) {
                throw new Error(authErrorDescription.ID_NOT_PROVIDED);
            }
            const topic = await AuthController.instance.authUseCase.getById(request.params.id)
            if (topic === undefined) {
                throw new Error(authErrorDescription.TOPIC_NOT_FOUND);
            }
            await AuthController.instance.authUseCase.delete(request.params.id)
            return await AuthController.instance.responseHandler.response("success", 202, topic)
        } catch (error: any) {
            return await AuthController.instance.errorHandler(error);
        }
    }

    async getAll(): Promise<ResponseModel<Auth[]>> {
        try {
            const topics = await AuthController.instance.authUseCase.getAll()
            return await AuthController.instance.responseHandler.response("success", 200, topics)
        } catch (error: any) {
            return await AuthController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    async getById(request: RequestModel<AuthParamsRequestInput>): Promise<ResponseModel<Auth>> {
        try {
            if (request.params?.id === undefined) {
                throw new Error(authErrorDescription.ID_NOT_PROVIDED)
            }
            const topic = await AuthController.instance.authUseCase.getById(request.params?.id);
            if (topic !== undefined) {
                return await AuthController.instance.responseHandler.response("success", 200, topic)
            } else {
                throw new Error(authErrorDescription.TOPIC_NOT_FOUND)
            }
        } catch (error: any) {
            if (error.message.includes(authErrorDescription.TOPIC_NOT_FOUND)) {
                return await AuthController.instance.responseHandler.response('error', 404, error.message);
            }
            return await AuthController.instance.responseHandler.response('error', 500, error.message);
        }
    }

    private async errorHandler(error: any): Promise<ResponseModel<Auth>> {
        let code: number = 500;
        let errorMessage = error.message
        if (errorMessage === authErrorDescription.TOPIC_NOT_FOUND || errorMessage === authErrorDescription.PARENT_TOPIC_NOT_FOUND) {
            code = 404;
        } else if (Object.values(authErrorDescription).includes(errorMessage)) {
            code = 400;
        } else if (errorMessage.includes("la sintaxis de entrada no es válida para tipo")) {
            code = 400;
            errorMessage = authErrorDescription.PARAM_WRONG_TYPE
        }
        return await AuthController.instance.responseHandler.response('error', code, errorMessage);
    }
}