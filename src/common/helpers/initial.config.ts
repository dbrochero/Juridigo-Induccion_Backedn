const validateEnvironmentVariable = (variable: string): string => {

    if(!process.env?.[variable]){
        console.error(`Environment variable ${variable} is required`);
        process.exit(1);
    }
    return process.env?.[variable]!;
}
export const APP_VARIABLES = {
    //DATABASE CONFIG
    DB_HOST:  validateEnvironmentVariable('DB_HOST'),
    DB_USERNAME:  validateEnvironmentVariable('POSTGRES_USER'),
    DB_PASSWORD:  validateEnvironmentVariable('POSTGRES_PASSWORD'),
    DB_NAME:  validateEnvironmentVariable('POSTGRES_DB'),
    DB_PORT:  validateEnvironmentVariable('DB_PORT'),
    //API's
    API: validateEnvironmentVariable('API'),
    //OTHERS
    APP_PORT: validateEnvironmentVariable('APP_PORT')
}
