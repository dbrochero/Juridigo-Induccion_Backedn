import {Column, Entity, OneToMany, ManyToOne, CreateDateColumn, DeleteDateColumn} from 'typeorm'
import { BaseEntities } from './common/base.entity'
import {AuthEntity} from "./auth.entity";
import {ItemEntity} from "./item.entity";

@Entity({name:"purchase"})
export class PurchaseEntity extends BaseEntities {

    @CreateDateColumn({
        name: "purchaseAt",
        type: "timestamp"
    })
    purchaseAt!: Date;

    @ManyToOne(() => AuthEntity, (auth) => auth.purchases)
    auth!: AuthEntity;

    @OneToMany(() => ItemEntity, (item) => item.purchases)
    item!: ItemEntity[];

};