import {Column, Entity, OneToMany, ManyToOne, DeleteDateColumn} from 'typeorm'
import { AuthEntity } from './auth.entity';

import { PurchaseEntity } from './purchase.entity';
import { BaseEntities } from './common/base.entity'

@Entity({name:"item"})
export class ItemEntity extends BaseEntities {

    @Column({length: 100, nullable: false, unique: true})
    title!: string

    @Column({length: 100, nullable: false})
    description!: string

    @Column({nullable: false})
    price!: number

    @Column({nullable: false})
    quantity!: number

    @DeleteDateColumn()
    deletedAt!: Date;

    @ManyToOne(() => AuthEntity, (auth) => auth.items, {nullable: false})
    auth!: AuthEntity;

    @ManyToOne(() => PurchaseEntity, (purchase) => purchase.item, {nullable: false})
    purchases!: PurchaseEntity;

};