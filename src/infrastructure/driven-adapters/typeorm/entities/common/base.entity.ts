import {
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm"

export class BaseEntities extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @CreateDateColumn({
        name: "createdAd",
        type: "timestamp"
    })
    createdAt!: Date;

    @UpdateDateColumn({
        name: "updatedAd",
        type: "timestamp",
    })
    updatedAt!: Date;

};