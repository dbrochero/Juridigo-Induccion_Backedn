import { Column, Entity, OneToMany } from 'typeorm'
import { ItemEntity } from './item.entity';

import { PurchaseEntity } from './purchase.entity';
import { BaseEntities } from './common/base.entity'

@Entity({name:"auth"})
export class AuthEntity extends BaseEntities {

    @Column({length: 100, nullable: false})
    firstName!: string

    @Column({length: 100})
    secondName!: string

    @Column({length: 100})
    lastName!: string

    @Column({length: 100})
    secondLastName!: string

    @Column({length: 100, nullable: false, unique: true})
    email!: string

    @Column({default: 0})
    balance!: number

    @OneToMany(() => ItemEntity, (item) => item.auth)
    items!: ItemEntity[];

    @OneToMany(() => PurchaseEntity, (purchase) => purchase.auth)
    purchases!: PurchaseEntity[];

};