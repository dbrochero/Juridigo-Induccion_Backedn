import { AuthEntity } from '../../../entities/auth.entity'
import { Auth } from "../../../../../../entities/models/auth/auth.model.entity";
import { PurchaseEntity } from "../../../entities/purchase.entity";
import { Purchase } from "../../../../../../entities/models/purchase/purchase.model.entity";
import { ItemEntity } from "../../../entities/item.entity";
import { Item } from "../../../../../../entities/models/item/item.model.entity";

export function authEntityToDomainTopic(authEntity: AuthEntity): Auth {
    return {
        id: authEntity.id,
        firstName: authEntity.firstName,
        lastName: authEntity.lastName,
        secondName: authEntity.secondName,
        secondLastName: authEntity.secondLastName,
        email: authEntity.email,
        balance: authEntity.balance,
        createAt: authEntity.createdAt,
        updateAt: authEntity.updatedAt
    }
}

export function itemEntityToDomainTopic(itemEntity: ItemEntity): Item {
    const { auth } = itemEntity;
    //Validate if auth exists
    let authId: number | undefined;
    if ( auth !== null && auth !== undefined ) {
        authId = auth.id;
    } else {
        authId = 0;
    }

    const outputItem: Item = {
        id: itemEntity.id,
        title: itemEntity.title,
        description: itemEntity.description,
        price: itemEntity.price,
        quantity: itemEntity.quantity,
        createAt: itemEntity.createdAt,
        updateAt: itemEntity.updatedAt,
        deletedAt: itemEntity.deletedAt
    }


    return outputItem
}

export function purchaseEntityToDomainTopic(purchaseEntity: PurchaseEntity): Purchase {
    const { auth, item } = purchaseEntity;

    // Validate if auth exits
    let authId: number | undefined;
    if ( auth !== null && auth !== undefined ) {
        authId = auth.id;
    } else {
        authId = 0;
    }

    const outputPurchase: Purchase = {
        id: purchaseEntity.id,
        createAt: purchaseEntity.createdAt,
        updateAt: purchaseEntity.updatedAt,
        purchaseAt: purchaseEntity.purchaseAt
    }

    if(authId !== 0){
        outputPurchase.authId = purchaseEntity.auth.id
    }

    return outputPurchase;

}