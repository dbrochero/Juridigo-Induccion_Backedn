import {Connection, EntityManager, Repository} from "typeorm";
import { IPurchaseRepository } from "../../../../application/ports/repositories/purchase/purchase.repository";
import { Purchase, PurchaseUpdateInput, PurchaseCreateInput } from "../../../../entities/models/purchase/purchase.model.entity";
import { PurchaseEntity } from "../entities/purchase.entity";
import { purchaseEntityToDomainTopic } from "./common/helpers/transform-data-to-domain";
import { GeneralRepository } from "./general.repository";

export class PurchaseRepository extends  GeneralRepository implements IPurchaseRepository {
    private repository: Repository<PurchaseEntity>;

    constructor(connection: Connection) {
        super(connection);
        this.repository = connection.getRepository(PurchaseEntity)
    }

    create(input: PurchaseCreateInput): Promise<Purchase>;
    create<T>(input: PurchaseCreateInput, transactionManager: T):Promise<Purchase>;
    async create(input: PurchaseCreateInput, transactionManager?: any): Promise<Purchase>{
        const { authId, purchaseAt, itemId } = input;
        let draftItem: PurchaseEntity = new PurchaseEntity();

        if (authId !== undefined && authId !== null){
            if (typeof authId !== "number" ){
                throw new Error(" Tipo equivocado")
            }
        }
        if (purchaseAt !== undefined && purchaseAt !== null){
            if (typeof purchaseAt !== ("string" || "Date" ) ){
                throw new Error(" Tipo equivocado")
            }
        }
        if (itemId !== undefined && itemId !== null){
            if (typeof itemId !== "number" ){
                throw new Error(" Tipo equivocado")
            }
        }

        if (transactionManager) {
            await transactionManager.save(draftItem)
        } else {
            await this.repository.save(draftItem)
        }
        return purchaseEntityToDomainTopic(draftItem);
    }

    update(id: number, input: PurchaseUpdateInput,): Promise<Purchase | undefined>;
    update<T>(id: number, transactionManager: T): Promise<Purchase | undefined>;
    async update(id: number, input: PurchaseUpdateInput, transactionManager?: EntityManager): Promise<Purchase | undefined> {
        let purchase = await this.repository.findOne(id);
        let draftItem: PurchaseEntity = new PurchaseEntity();

        const { itemId, updateAt, authId } = input;
        if (itemId !== undefined && itemId !== null){
            if (typeof itemId !== "number" ){
                throw new Error(" Tipo equivocado")
            }
        }
        if (updateAt !== undefined && updateAt !== null){
            if (typeof updateAt !== ("string" || "Date") ){
                throw new Error(" Tipo equivocado")
            }
        }
        if (authId !== undefined && authId !== null){
            if (typeof authId !== "number" ){
                throw new Error(" Tipo equivocado")
            }
            //draftItem.auth = authId;
        }

        return purchaseEntityToDomainTopic(draftItem);
    }

    delete(id: number): Promise<Purchase | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Purchase | undefined>;
    async delete(id: number, transactionManager?: any): Promise<Purchase | undefined> {
        const purchase = await this.repository.findOne(id)
        if (purchase === undefined || purchase === null) {
            throw new Error(" Error de tipo")
        }
        if (transactionManager) {
            await transactionManager.softRemove(purchase)
        } else {
            await this.repository.softRemove(purchase)
        }
        return purchaseEntityToDomainTopic(purchase)
    }

    async getById(id: number): Promise<Purchase | undefined> {
        try {
            const auth = await this.repository.findOne(id)
            if (auth === undefined || auth === null) {
                throw new Error("Error ")
            }
            return purchaseEntityToDomainTopic(auth)
        } catch (error: any) {

        }
    }

    async getAll(): Promise<Array<Purchase>> {
        const items = await this.repository.find()
        return items.map(item => purchaseEntityToDomainTopic(item))
    }

}