import {Connection, EntityManager, Repository} from "typeorm";
import { IAuthRepository } from "../../../../application/ports/repositories/auth/auth.repository";
import { Auth, AuthUpdateInput, AuthCreateInput } from "../../../../entities/models/auth/auth.model.entity";
import { AuthEntity } from "../entities/auth.entity";
import { authEntityToDomainTopic } from "./common/helpers/transform-data-to-domain";
import { GeneralRepository } from "./general.repository";

export class AuthRepository extends  GeneralRepository implements IAuthRepository {
    private repository: Repository<AuthEntity>;

    constructor(connection: Connection) {
        super(connection);
        this.repository = connection.getRepository(AuthEntity)
    }

    create(input: AuthCreateInput): Promise<Auth>;
    create<T>(input: AuthCreateInput, transactionManager: T):Promise<Auth>;
    async create(input: AuthCreateInput, transactionManager?: any): Promise<Auth>{
        console.log("Create user")
        const { firstName, secondName, lastName, secondLastName, email } = input;
        let draftItem: AuthEntity = new AuthEntity();

        if (firstName !== undefined && firstName !== null){
            if (typeof firstName !== "string" ){
                throw new Error(" Tipo equivocado")
            }
            if(firstName.length > 100){
                throw new Error(" Longitud excedida ")
            }
        }
        if (secondName !== undefined && secondName !== null) {
            if(typeof secondName !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(secondName.length > 100){
                throw new Error("Longitud excedida ")
            }
            draftItem.secondName = secondName;
        }
        if (lastName !== undefined && lastName !== null) {
            if(typeof lastName !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(lastName.length > 100){
                throw new Error(" Longitud excedida ")
            }
            draftItem.lastName = lastName;
        }
        if (secondLastName !== undefined && secondLastName !== null) {
            if(typeof secondLastName !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(secondLastName.length > 100){
                throw new Error(" Longitud excedida ")
            }
            draftItem.secondLastName = secondLastName;
        }
        if (email !== undefined && email !== null) {
            if(typeof email !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(email.length > 100){
                throw new Error(" Longitud excedida ")
            }
            draftItem.email = email;
        }

        if (transactionManager) {
            await transactionManager.save(draftItem)
        } else {
            await this.repository.save(draftItem)
        }
        return authEntityToDomainTopic(draftItem);
    }

    update(id: number, input: AuthUpdateInput,): Promise<Auth | undefined>;
    update<T>(id: number, transactionManager: T): Promise<Auth | undefined>;
    async update(id: number, input: AuthUpdateInput, transactionManager?: EntityManager): Promise<Auth | undefined> {
        let item = await this.repository.findOne(id);
        let draftItem: AuthEntity = new AuthEntity();
        if(item === undefined){
            throw new Error(" Tipo equivocado")
        }
        const { firstName, secondName, lastName, secondLastName, email } = input;
        if (firstName !== undefined && firstName !== null){
            if (typeof firstName !== "string" ){
                throw new Error(" Tipo equivocado")
            }
            if(firstName.length > 100){
                throw new Error(" Longitud excedida ")
            }
        }
        if (secondName !== undefined && secondName !== null) {
            if(typeof secondName !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(secondName.length > 100){
                throw new Error("Longitud excedida ")
            }
            draftItem.secondName = secondName;
        }
        if (lastName !== undefined && lastName !== null) {
            if(typeof lastName !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(lastName.length > 100){
                throw new Error(" Longitud excedida ")
            }
            draftItem.lastName = lastName;
        }
        if (secondLastName !== undefined && secondLastName !== null) {
            if(typeof secondLastName !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(secondLastName.length > 100){
                throw new Error(" Longitud excedida ")
            }
            draftItem.secondLastName = secondLastName;
        }
        if (email !== undefined && email !== null) {
            if(typeof email !== "string"){
                throw new Error(" Tipo equivocado")
            }
            if(email.length > 100){
                throw new Error(" Longitud excedida ")
            }
            draftItem.email = email;
        }
        if (transactionManager) {
            await transactionManager.save(draftItem)
        } else {
            await this.repository.save(draftItem)

        }
        return authEntityToDomainTopic(draftItem);
    }

    delete(id: number): Promise<Auth | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Auth | undefined>;
    async delete(id: number, transactionManager?: any): Promise<Auth | undefined> {
        const item = await this.repository.findOne(id)
        if (item === undefined || item === null) {
            throw new Error(" Error de tipo")
        }
        if (transactionManager) {
            await transactionManager.softRemove(item)
        } else {
            await this.repository.softRemove(item)
        }
        return authEntityToDomainTopic(item)
    }

    async getById(id: number): Promise<Auth | undefined> {
        try {
            const auth = await this.repository.findOne(id)
            if (auth === undefined || auth === null) {
                throw new Error("Error ")
            }
            return authEntityToDomainTopic(auth)
        } catch (error: any) {

        }
    }

    async getAll(): Promise<Array<Auth>> {
        const items = await this.repository.find()
        return items.map(item => authEntityToDomainTopic(item))
    }

    async getByEmail(email: string): Promise<Auth | undefined> {
        const auth = await this.repository.findOne(email);
        if(auth){
            return authEntityToDomainTopic(auth);
        }
    }

}