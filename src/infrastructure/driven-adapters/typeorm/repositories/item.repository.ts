import {Connection, EntityManager, Repository} from "typeorm";
import { IItemRepository } from "../../../../application/ports/repositories/item/item.repository";
import { Item, ItemUpdateInput, ItemCreateInput } from "../../../../entities/models/item/item.model.entity";
import { ItemEntity } from "../entities/item.entity";
import { itemEntityToDomainTopic } from "./common/helpers/transform-data-to-domain";
import { GeneralRepository } from "./general.repository";

export class ItemRepository extends  GeneralRepository implements IItemRepository {
    private repository: Repository<ItemEntity>;

    constructor(connection: Connection) {
        super(connection);
        this.repository = connection.getRepository(ItemEntity)
    }

    create(input: ItemCreateInput): Promise<Item>;
    create<T>(input: ItemCreateInput, transactionManager: T):Promise<Item>;
    async create(input: ItemCreateInput, transactionManager?: any): Promise<Item>{
        const { title, description, price, quantity } = input;
        let draftItem: ItemEntity = new ItemEntity();
        if (title !== undefined ){
            if (typeof title !== "string" ){
                throw new Error(" Tipo equivocado")
            }
            draftItem.title = title;
        }
        if (description !== undefined) {
            if(typeof description !== "string"){
                throw new Error(" Tipo equivocado")
            }
            draftItem.description = description;
        }
        if (price !== undefined) {
            if(typeof description !== "number"){
                throw new Error(" Tipo equivocado")
            }
            draftItem.price = price;
        }
        if (quantity !== undefined) {
            if(typeof description !== "number"){
                throw new Error(" Tipo equivocado")
            }
            draftItem.quantity = quantity;
        }
        if (transactionManager) {
            await transactionManager.save(draftItem)
        } else {
            await this.repository.save(draftItem)
        }
        return itemEntityToDomainTopic(draftItem);
    }

    update(id: number, input: ItemUpdateInput,): Promise<Item | undefined>;
    update<T>(id: number, transactionManager: T): Promise<Item | undefined>;
    async update(id: number, input: ItemUpdateInput, transactionManager?: EntityManager): Promise<Item | undefined> {
        let item = await this.repository.findOne(id);
        let draftItem: ItemEntity = new ItemEntity();
        if(item === undefined){
            throw new Error(" Tipo equivocado")
        }
        const { title, description, price, quantity } = input;
        if (title != undefined){
            if (typeof title !== "string") {
                throw new Error(" Tipo equivocado")
            }
            draftItem.title = title;
        }
        if (description != undefined){
            if (typeof title !== "string") {
                throw new Error(" Tipo equivocado")
            }
            draftItem.description = description;
        }
        if (price != undefined){
            if (typeof price !== "number") {
                throw new Error(" Tipo equivocado")
            }
            draftItem.price = price;
        }
        if (quantity != undefined){
            if (typeof quantity !== "number") {
                throw new Error(" Tipo equivocado")
            }
            draftItem.quantity = quantity;
        }
        if (transactionManager) {
            await transactionManager.save(draftItem)
        } else {
            await this.repository.save(draftItem)

        }
        return itemEntityToDomainTopic(draftItem);


    }

    delete(id: number): Promise<Item | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<Item | undefined>;
    async delete(id: number, transactionManager?: any): Promise<Item | undefined> {
        const item = await this.repository.findOne(id)
        if (item === undefined || item === null) {
            throw new Error(" Error de tipo")
        }
        if (transactionManager) {
            await transactionManager.softRemove(item)
        } else {
            await this.repository.softRemove(item)
        }
        return itemEntityToDomainTopic(item)
    }

    async getById(id: number): Promise<Item | undefined> {
        try {
            const item = await this.repository.findOne(id)
            if (item === undefined || item === null) {
                throw new Error("Error ")
            }
            return itemEntityToDomainTopic(item)
        } catch (error: any) {

        }
    }

    async getAll(): Promise<Array<Item>> {
        const items = await this.repository.find()
        return items.map(item => itemEntityToDomainTopic(item))
    }

    async getByTitle(title: string): Promise<Item | undefined> {
        const items = await this.repository.findOne(title);
        if(items){
            return itemEntityToDomainTopic(items);
        }
    }

}