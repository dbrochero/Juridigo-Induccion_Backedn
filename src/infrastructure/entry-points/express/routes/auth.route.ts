import { createAuthControllerFactory } from "../../../../main/factories/controllers/auth.controller.factory";
import { Router } from "express";
import { routeAdapter } from "../adapters/router-adapter";

const authRouter = () => {
    console.log("routes auth routes")
    const router = Router();
    const { authController } = createAuthControllerFactory();
    router.route("/")
        .get(routeAdapter(authController.getAll))
        .post(routeAdapter(authController.create))
    router.route("/:id")
        .patch(routeAdapter(authController.update))
        .delete(routeAdapter(authController.delete))
        .get(routeAdapter(authController.getById))
    return router
}

export {
    authRouter
}