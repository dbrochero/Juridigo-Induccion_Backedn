import { createItemControllerFactory } from "../../../../main/factories/controllers/item.controller.factory";
import { Router } from "express";
import { routeAdapter } from "../adapters/router-adapter";

const itemRouter = () => {
    const router = Router();
    const { itemController } = createItemControllerFactory();
    router.route("/")
        .get(routeAdapter(itemController.getAll))
        .post(routeAdapter(itemController.create))
    router.route("/:id")
        .patch(routeAdapter(itemController.update))
        .delete(routeAdapter(itemController.delete))
        .get(routeAdapter(itemController.getById))
    return router
}

export {
    itemRouter
}