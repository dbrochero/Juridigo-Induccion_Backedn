import { createPurchaseControllerFactory } from "../../../../main/factories/controllers/purchase.controller.factory";
import { Router } from "express";
import { routeAdapter } from "../adapters/router-adapter";

const purchaseRouter = () => {
    const router = Router();
    const { purchaseController } = createPurchaseControllerFactory();
    router.route("/")
        .get(routeAdapter(purchaseController.getAll))
        .post(routeAdapter(purchaseController.create))
    router.route("/:id")
        .patch(routeAdapter(purchaseController.update))
        .delete(routeAdapter(purchaseController.delete))
        .get(routeAdapter(purchaseController.getById))
    return router
}

export {
    purchaseRouter
}