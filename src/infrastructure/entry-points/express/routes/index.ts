import {Router} from 'express';
import {authRouter} from './auth.route';
import {itemRouter} from "./item.route";
import {purchaseRouter} from "./purchase.route";

const routes = () => {
    const router = Router();
    router.use('/auths', authRouter());
    router.use("/items", itemRouter());
    router.use("/purchases", purchaseRouter());
    return router;
};


export {
    routes
};
