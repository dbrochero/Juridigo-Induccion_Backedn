import { getDbConnection } from "../../../infrastructure/driven-adapters/typeorm/config/db.config";
import { AuthRepository } from "../../../infrastructure/driven-adapters/typeorm/repositories/auth.repository";
import { AuthUseCase } from "../../../application/use-cases/auth/auth.use-case";
import { AuthController } from "../../../presentation/controllers/auth/auth.controller";
import { GeneralResponseHandler} from "../../../presentation/responses/general-response-handler.adapter";

export function createAuthControllerFactory() {
    console.log("DDDDDDDDD")
    const dbConnection = getDbConnection();
    const authRepository = new AuthRepository(dbConnection);
    const authUseCase = new AuthUseCase(authRepository);
    const authController = new AuthController(authUseCase, new GeneralResponseHandler());
    return {
        authRepository,
        authUseCase,
        authController
    }
}
