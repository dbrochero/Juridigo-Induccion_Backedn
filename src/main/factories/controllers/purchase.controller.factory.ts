import { getDbConnection } from "../../../infrastructure/driven-adapters/typeorm/config/db.config";
import { PurchaseRepository } from "../../../infrastructure/driven-adapters/typeorm/repositories/purchase.repository";
import { PurchaseUseCase } from "../../../application/use-cases/purchase/purchase.use-case";
import { PurchaseController } from "../../../presentation/controllers/purchase/purchase.controller";
import { GeneralResponseHandler} from "../../../presentation/responses/general-response-handler.adapter";

export function createPurchaseControllerFactory() {
    const dbConnection = getDbConnection();
    const purchaseRepository = new PurchaseRepository(dbConnection);
    const purchaseUseCase = new PurchaseUseCase(purchaseRepository);
    const purchaseController = new PurchaseController(purchaseUseCase, new GeneralResponseHandler());
    return {
        purchaseRepository,
        purchaseUseCase,
        purchaseController
    }
}
