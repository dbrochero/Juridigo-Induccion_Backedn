import { getDbConnection } from "../../../infrastructure/driven-adapters/typeorm/config/db.config";
import { ItemRepository } from "../../../infrastructure/driven-adapters/typeorm/repositories/item.repository";
import { ItemUseCase } from "../../../application/use-cases/item/item.use-case";
import { ItemController } from "../../../presentation/controllers/item/item.controller";
import { GeneralResponseHandler} from "../../../presentation/responses/general-response-handler.adapter";

export function createItemControllerFactory() {
    const dbConnection = getDbConnection();
    const itemRepository = new ItemRepository(dbConnection);
    const itemUseCase = new ItemUseCase(itemRepository);
    const itemController = new ItemController(itemUseCase, new GeneralResponseHandler());
    return {
        ItemRepository,
        ItemUseCase,
        itemController
    }
}
